﻿namespace dz_pattern_prototype
{
    public class ParseResultItem
    {
        public string Title { get; set; }
        public string Excerp { get; set; }
    }
}