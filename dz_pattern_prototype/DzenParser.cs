﻿using System;
using System.Collections.Generic;
using Leaf.xNet;
using HtmlAgilityPack;

namespace dz_pattern_prototype
{
    public class DzenParser: ICloneable
    {
        protected HtmlNode hnRoot;
        protected HtmlDocument htmlDoc;
        protected string url;
        protected List<ParseResultItem> ResultItems { get; set;}

        public virtual void SaveResult()
        {
            foreach (var item in ResultItems)
            {
                Console.WriteLine($"Цитата из статьи:{item.Excerp}\n");
            }
        }

        public object Clone()
        {
            var dp = new DzenParser();
            return dp;
        }

        public DzenParser()
        {
            ResultItems = new List<ParseResultItem>();
        }

        public string GetContent()
        {
            HttpRequest http = new HttpRequest();
            http.UserAgentRandomize();
            
           
            HttpResponse response = null;

            response = http.Get(url);
            return  response.ToString();
        }
        public virtual void ParseDo(string parseUrl)
        {
            url = parseUrl;
            string content = GetContent();
            
         //  HtmlNode hnTitle;
            HtmlNodeCollection hcArticlesBlocks;
            htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(content);
           
            hnRoot = htmlDoc.DocumentNode;

            hcArticlesBlocks = hnRoot.SelectNodes(@"//a[@class='card-image-view__clickable']//following::div[@class=""card-content__text-content""]");

            ParseResultItem resultItem = null;
            foreach (var item  in hcArticlesBlocks.Nodes())
            {
                resultItem = new ParseResultItem();
                resultItem.Excerp = item.InnerText;
                this.ResultItems.Add(resultItem);
            }
        }

    }
}