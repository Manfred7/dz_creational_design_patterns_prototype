﻿using System;
using System.Resources;

namespace dz_pattern_prototype
{
    public interface IRepo
    {
        void DoSave(ParseResultItem item);
    }
    public class dbRepository:IRepo
    {
        public void DoSave(ParseResultItem item)
        {
            Console.WriteLine($"Сохраняем Цитата из статьи:{item.Excerp}\n");
        }

    }
}