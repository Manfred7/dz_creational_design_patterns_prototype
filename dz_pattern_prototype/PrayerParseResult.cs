﻿using System;
using System.IO;
using System.Text;

namespace dz_pattern_prototype
{
    public class PrayerParseResult
    {
        const string cFileName = @"\text.txt";
        public string Title { get; set; }
        public string Text { get; set; }
        
        public string H1 { get; set; }
        public void SaveData(string rootDir)
        {
            string prayerDir = CreatePrayerDir(rootDir);
            SaveToTextFile(prayerDir);
        }
        private void SaveToTextFile(string Dir)
        {
            string sFileName = Dir + cFileName;
            try
            {
                File.AppendAllText(sFileName, this.Text, Encoding.UTF8);
                Console.WriteLine($"Сохранили файл: {sFileName}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private string CreatePrayerDir(string rootDir)
        {
            string newDir = String.Empty;
            try
            {
                newDir = Path.Combine(rootDir, this.Title);
                Directory.CreateDirectory(newDir);
                return newDir;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}