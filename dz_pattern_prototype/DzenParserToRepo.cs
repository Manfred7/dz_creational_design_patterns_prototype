﻿namespace dz_pattern_prototype
{
    public class DzenParserToRepo: DzenParser
    {
        private IRepo rp { get; set; }

        public DzenParserToRepo(IRepo repo) : base()
        {
            rp = repo;
        }
        
        public override void SaveResult()
        {
            foreach (var item in ResultItems)
            {
                rp.DoSave(item);
            }
        }
        
    }
}