﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace dz_pattern_prototype
{
    public class PrayerParser2 : PrayerParser
    {
        const string cFileName = @"textJsoin.txt";
        public PrayerParser2(string h1XPath, string textXPath) : base(h1XPath, textXPath)
        {
            // 
        }

        public override void SaveResult()
        {
            string rootDir = AppContext.BaseDirectory;

            foreach (var item in ResultItems)
            {
                string json = JsonConvert.SerializeObject(item, Formatting.Indented);
                SaveToTextFile(rootDir, json);
            }
        }

        public override BaseParser getClone()
        {
            var bp = new PrayerParser2(this.H1XPath, this.TextXPath);
            return bp;
        }

        private void SaveToTextFile(string Dir, string value)
        {
            string sFileName = Dir + cFileName;
            try
            {
                File.AppendAllText(sFileName, value);
                Console.WriteLine($"Сохранили файл: {sFileName}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}