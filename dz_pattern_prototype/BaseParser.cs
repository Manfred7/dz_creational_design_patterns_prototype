﻿using System;
using System.Collections.Generic;
using HtmlAgilityPack;
using Leaf.xNet;

namespace dz_pattern_prototype
{
    interface IMyParserCloneable
    {
        BaseParser getClone();
    }

    public class BaseParser : ICloneable, IMyParserCloneable
    {
        protected HtmlNode hnRoot;
        protected HtmlDocument htmlDoc;
        protected string url;

        public string H1XPath { get; set; }
        public string TextXPath { get; set; }

        public BaseParser(string h1XPath, string textXPath)
        {
            H1XPath =h1XPath;
            TextXPath = textXPath;
        }

        public object Clone()
        {
            return getClone();
        }

        public virtual void SaveResult()
        {
            //
        }
        public virtual BaseParser getClone()
        {
            var bp = new BaseParser(this.H1XPath,this.TextXPath);
            return bp;
        }

        public string GetContent()
        {
            HttpRequest http = new HttpRequest();
            http.UserAgentRandomize();

            HttpResponse response = http.Get(url);
            return response.ToString();
        }

        public virtual void ParseDo(string parseUrl)
        {
            //
        }
    }
}