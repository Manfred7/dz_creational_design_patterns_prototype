﻿using System;

namespace dz_pattern_prototype
{
    interface IMyCloneable
    {
        Circle getClone();
    }

    public class Circle : IMyCloneable, ICloneable
    {
        private string color;
        private int radius;

        public Circle(int radius, string color)
        {
            this.radius = radius;
            this.color = color;
        }

        public object Clone()
        {
            return getClone();
        }

        public Circle getClone()
        {
            return new Circle(radius, color);
        }

        public override string ToString()
        {
            return $"radius={radius}; color= {color}";
        }
    }
}