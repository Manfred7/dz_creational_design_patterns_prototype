﻿using System;
using SitemapGraber;

namespace dz_pattern_prototype
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            // настройки парсинга, из каких тэгов брать данные, могут грузится из репозитория с настройками
            string H1XPath = @"//h1";
            string TextXPath = @"//div[contains(@class, 'block-molitva')]/div[contains(@class, 'molitva-content')]";
            string parseUrl = @"http://mymolitva.ru/molitva-borisu-i-glebu/";

            BaseParser m = new PrayerParser(H1XPath,TextXPath); //Этот создает папки и отдельные текстовые файлы
            m.ParseDo(parseUrl);
            m.SaveResult();

            parseUrl = @"http://mymolitva.ru/molitvy-v-dorogu-i-pered-dorogoy/";
            BaseParser m2 = m.getClone();
            m2.ParseDo(parseUrl);
            m2.SaveResult();
            
            parseUrl = @"http://mymolitva.ru/molitvy-varvare-velikomuchenice/";
            BaseParser m3 = new PrayerParser2(H1XPath,TextXPath); //Этот сохраняет в 1 текстовый файл в формате JSON
            m3.ParseDo(parseUrl);
            m3.SaveResult();
            
            parseUrl = @"http://mymolitva.ru/molitvy-daniilu-moskovskomu/";
            BaseParser m4 = m3.getClone();
            m4.ParseDo(parseUrl);
            m4.SaveResult();
        }

        private static void GetSitampXml()
        {
            string sUrl = @"http://mymolitva.ru/sitemap.xml";
            SitemapGrab Graber = new SitemapGrab(sUrl);
            Graber.DoGrab();
            Graber.SaveToTextFile(AppContext.BaseDirectory, "molitv");
        }

      
    }
}