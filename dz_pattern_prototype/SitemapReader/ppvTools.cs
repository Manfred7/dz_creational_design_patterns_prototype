﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Leaf.xNet;


namespace SitemapGraber
{
    class ppvTools
    {  
        public static Stream GenerateStreamFromString(string s)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        public static string GET(string Url)
        {
            HttpRequest http = new HttpRequest();
            http.UserAgentRandomize();
            HttpResponse response = http.Get(Url);
            return  response.ToString();
        }
    }
}
