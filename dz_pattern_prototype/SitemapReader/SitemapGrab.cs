﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using SitemapStandart;

namespace SitemapGraber
{
    public class SitemapGrab
    {
        private string MapLink;
        private SitemapStandart.Sitemapindex smp;       
        private SitemapStandart.Urlset links;        
        public SitemapStandart.Urlset resultUrlSet;
        public SitemapGrab(string spLink)
        {
            MapLink = spLink;
            resultUrlSet = new SitemapStandart.Urlset();
        }
        private void DeserializeMap(string sMap)
        {
            DeserializeObj<Sitemapindex>(sMap, ref smp);           
        }
        private void DeserializeLinks(string sMap)
        {        
            DeserializeObj<Urlset>(sMap, ref links);          
        }

        //Параметрический полиморфизм
        private void DeserializeObj<T>(string text, ref T obj)
        {
            using (var stream = ppvTools.GenerateStreamFromString(text))
            {
                XmlSerializer formatter = new XmlSerializer(typeof(T));
                obj = (T)formatter.Deserialize(stream);
            }
        }
        // выводим в консоль урлы статей из карты сайта
        public void ShowLinks()
        {
            foreach (Url tempLink in resultUrlSet.Url)
            {
                Console.WriteLine(tempLink.Loc);
            }
        }

        public void SaveToTextFile(string rootDir, string contains_filter)
        {
            string sFileName = rootDir + @"SitemapLinks.txt";
            try
            {
                string result = String.Empty;

               // File.Create(sFileName);

               var f = resultUrlSet.Url.ToList();

               foreach (Url tempLink in resultUrlSet.Url)
               {
                   if (tempLink.Loc.Contains(contains_filter))
                   {
                       result =tempLink.Loc + Environment.NewLine;
                       File.AppendAllText(sFileName, result, Encoding.UTF8);
                       
                    }
               }
               Console.WriteLine($"Сохранили файл: {sFileName}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public void DoGrab()
        {
            string resultGet;

            try
            {
                // получаем карту сайта
                resultGet = ppvTools.GET(MapLink);

                //десериализуем карту сайта
                DeserializeMap(resultGet);

                //по стандарту карта сайта содержит вложенные карты извлечем их 
                foreach(Sitemap tmpSml in smp.Sitemap)
                {          
                    // получаем вложенную карту сайта
                    resultGet = ppvTools.GET(tmpSml.Loc);

                    //выводим в консоль урл вложенной карты сайта
                    Console.WriteLine(tmpSml.Loc);

                    //десериализуем
                    DeserializeLinks(resultGet);

                    // добавим собранные линки в итоговый результат
                    resultUrlSet = resultUrlSet + links;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
