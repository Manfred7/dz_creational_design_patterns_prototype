﻿using System;
using System.Collections.Generic;
using System.Linq;
using Leaf.xNet;
using HtmlAgilityPack;
using System.IO;


namespace dz_pattern_prototype
{
    public class PrayerParser: BaseParser
    {
        protected List<PrayerParseResult> ResultItems { get; set;}
        private string internalDir { get; set; }

        public PrayerParser(string h1XPath, string textXPath):base(h1XPath,textXPath)
        {
            ResultItems = new List<PrayerParseResult>();
        }
        public override BaseParser getClone()
        {
            var bp = new PrayerParser(this.H1XPath,this.TextXPath);
            return bp;
        }
        public override void ParseDo(string parseUrl)
        {
            url = parseUrl;
            string content = GetContent();
      
            htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(content);
           
            hnRoot = htmlDoc.DocumentNode;
            HtmlNode hnH1; 
            HtmlNode hnTitle;  
            HtmlNode hnText;

            hnH1 = hnRoot.SelectSingleNode(H1XPath);
            internalDir = hnH1.InnerText;

           var hcArticlesBlocks = hnRoot.SelectNodes(this.TextXPath).ToList();

            PrayerParseResult resultItem = null;
            foreach (var item  in hcArticlesBlocks)
            {
                resultItem = new PrayerParseResult();

                hnTitle = item.ChildNodes.FindFirst(@"h3");
                hnText = item;

                resultItem.H1 = hnH1.InnerText;
                resultItem.Title = hnTitle.InnerText;
                resultItem.Text = hnText.InnerText;
                
                this.ResultItems.Add(resultItem);
            }
        }
        public override void SaveResult()
        {
            string rootDir = AppContext.BaseDirectory;
           // string molitvaDir = String.Empty;

            string localRoot = rootDir + internalDir;
            Directory.CreateDirectory(localRoot);// создаем для всех молитва в статье общую папку
            ;
            foreach (var item in ResultItems)
            {
                item.SaveData(localRoot);
                Console.WriteLine($"Сохранили: {item.Title}\n");
            }
        }

       
    }
    
    
}